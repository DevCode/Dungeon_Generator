#pragma once

#include <array>
#include <vector>
#include <map>
#include <functional>

#include "polygon.h"
#include "delaunay_triang.h"

struct edge {
    std::array<unsigned int, 2> vertex;
    float weight;

    bool operator==(const edge &e) const {
        return common_points(e);

    }

    bool operator!=(const edge &e) const {
        return !(*this == e);
    }

    bool operator<(const edge &e) const {
        return weight < e.weight;
    }

    bool operator>(const edge &e) const {
        return weight > e.weight;
    }

    unsigned int operator[](unsigned int index) const {
        return vertex[index];
    }

    unsigned int &operator[](unsigned int index) {
        return vertex[index];
    }

    int common_points(const edge &e) const {
        return (vertex[0] == e.vertex[0]
            || vertex[0] == e.vertex[1]) +
            (vertex[1] == e.vertex[0]
            || vertex[1] == e.vertex[1]);
    }

    int sharing_index(const edge &e) {
        if (!common_points(e))
            return -1;

        return !(vertex[0] == e[0] || vertex[0] == e[1]);
    }
};

class path {
    public :
        std::vector<edge> elist;
        std::vector<point> plist;

        void generate_paths(triangulation &triang);
};

struct room : public polygon<2> {
    room(std::initializer_list<vec2> points);
    room(const polygon<2> &pol);
};

struct link {
    size_t room_indices[2];
};

class map {
    public :
        std::vector<room> rlist;
        std::vector<room> plist;
        std::vector<link> llist;
        const unsigned int size_unit, nbr_of_nodes;

        map(unsigned int size, unsigned int max_nbr_of_nodes);
        void regenerate();
    private :
        void generate_rooms();
        void build_paths(path &p);
};
