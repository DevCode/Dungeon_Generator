#include <cstdio>
#include <ctime>
#include <iostream>

#include "SDL2/SDL.h"
#include "SDL2/SDL2_gfxPrimitives.h"

#include "generator.h"
#include "delaunay_triang.h"

#define NBR_OF_ROOMZ	64
#define WIDTH		1440
#define HEIGHT		1080

int main() {
    SDL_Window *window;
    SDL_Renderer *renderer;
    bool quit = false;
    bool hide_room = false;

    SDL_Init(SDL_INIT_EVERYTHING);

    window = SDL_CreateWindow("Dungeon Generator", 0, 0, WIDTH, HEIGHT, 0);
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

    map m(WIDTH * 0.5f, NBR_OF_ROOMZ);
    vec2 translation = {(float) WIDTH / 2, (float) HEIGHT / 2};

    while (!quit) {

        SDL_Event event;

        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT)
                quit = true;

            switch(event.type) {
                case SDL_KEYDOWN :
                    switch (event.key.keysym.scancode) {
                        case SDL_SCANCODE_G :
                            m.regenerate();
                            break;
                        case SDL_SCANCODE_H :
                            hide_room = !hide_room;
                            break;
                        case SDL_SCANCODE_ESCAPE :
                            quit = true;
                            break;
                        default :
                            break;
                    }
                    break;
                case SDL_MOUSEMOTION :
                    if (event.motion.state & SDL_BUTTON_LMASK) {
                        translation[0] += event.motion.xrel;
                        translation[1] += event.motion.yrel;
                    }
                    break;
                case SDL_QUIT :
                    quit = true;
                default :
                    break;
            }
        }

        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        SDL_RenderClear(renderer);

        if (!hide_room)
            for (room current_room : m.rlist) {
                for (int j = 0; j < current_room.number_of_vertices(); j++) {
                    int next = (j + 1) % current_room.number_of_vertices();
                    vec2 start = current_room[j] + translation;
                    vec2 end = current_room[next] + translation;

                    SDL_SetRenderDrawColor(renderer, 122, 255, 0, 255);
                    SDL_RenderDrawLine(renderer,
                            (unsigned int) (start[0]),
                            (unsigned int) (start[1]),
                            (unsigned int) (end[0]),
                            (unsigned int) (end[1]));
                }
            }

        for (room current_room : m.plist) {
            for (int j = 0; j < current_room.number_of_vertices(); j++) {
                    int next = (j + 1) % current_room.number_of_vertices();
                    vec2 start = current_room[j] + translation;
                    vec2 end = current_room[next] + translation;

                    SDL_SetRenderDrawColor(renderer, 122, 0, 0, 255);
                    SDL_RenderDrawLine(renderer,
                            (unsigned int) (start[0]),
                            (unsigned int) (start[1]),
                            (unsigned int) (end[0]),
                            (unsigned int) (end[1]));
            }
        }

        SDL_RenderPresent(renderer);
        SDL_Delay(16);
    }
    return 0;
}
