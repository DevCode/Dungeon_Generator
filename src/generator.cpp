#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cstdint>

#include <vector>
#include <random>
#include <algorithm>
#include <limits>
#include <iostream>

#include "generator.h"

room::room(std::initializer_list<vec2> points) : polygon(points) { }

room::room(const polygon<2> &pol) : polygon(pol) { }

map::map(unsigned int size_unit, unsigned int nbr_of_nodes)
    : size_unit(size_unit), nbr_of_nodes(nbr_of_nodes) {
        regenerate();
}

void map::regenerate() {
    rlist.clear();
    llist.clear();
    plist.clear();

    generate_rooms();

    std::vector<point> point_list;

    for (int i = 0; i < nbr_of_nodes; i++) {
        point_list.push_back(rlist[i][0] + ((rlist[i][2] - rlist[i][0]) * 0.5f));
    }

    triangulation triang;
    triang.triangulize(point_list);
    path paths;
    paths.generate_paths(triang);

    build_paths(paths);
}

void map::generate_rooms() {
    std::random_device rd;
    std::default_random_engine ran_engine(rd());
    std::uniform_real_distribution<float> ran_radian(0, 2 * M_PI);
    std::normal_distribution<float> ran_dist(size_unit * 0.25f, size_unit * 0.75f);
    std::exponential_distribution<float> ran_size(1 / (sqrt(size_unit) * 1.5f));
    std::uniform_real_distribution<float> ran_scale(0.5f, 2.0f);

    for (int i = 0; i < nbr_of_nodes; i++) {

        float rad = ran_radian(ran_engine);
        float dist = size_unit * 0.75f - ran_dist(ran_engine);
        float dir_x = cos(rad);
        float dir_y = sin(rad);

        float pos_x = dir_x * dist;
        float pos_y = dir_y * dist;

        float rwidth = ran_size(ran_engine) + (size_unit * 0.075);
        float rheight = rwidth * ran_scale(ran_engine);

        room new_room = {       {pos_x , pos_y},
                                {pos_x + rwidth, pos_y},
                                {pos_x + rwidth, pos_y + rheight},
                                {pos_x, pos_y + rheight} };

        // as reference because new_room changes
        auto collides = [&new_room](const room &curr) {
            return new_room.collides(curr);
        };

        while (any_of(rlist.cbegin(), rlist.cend(), collides))
            new_room.translate({dir_x * 20, dir_y * 20});

        rlist.push_back(new_room);
    }
}

void map::build_paths(path &p) {
    for (std::vector<edge>::size_type i = 0; i < p.elist.size(); ) {
        edge current_edge = p.elist[i];

        point start = p.plist[current_edge[0]];
        point end = p.plist[current_edge[1]];

        float scale = size_unit * 0.0075f;

        vec2 connection = end - start;
        connection.norm();

        connection = connection.get_ortho_vec();

        vec2 p1 = start - connection * scale;
        vec2 p2 = start + connection * scale;
        vec2 p3 = end + connection * scale;
        vec2 p4 = end - connection * scale;

        room new_path({p1, p2, p3, p4});

        room r1 = rlist[current_edge[0]];
        room r2 = rlist[current_edge[1]];

        auto colliding_room = std::find_if(rlist.begin(), rlist.end(),
                [&r1, &r2, &new_path](const room &r) {
                    if (r == r1 || r == r2)
                        return false;
                    return r.collides(new_path);
                });

        if (colliding_room != rlist.end()) {
            // for some reason std::distance returns some strange value
            // std::distance also returns NEGATIVE values
            unsigned int colliding_index = std::abs(std::distance(colliding_room, rlist.begin()));
            p.elist.push_back({colliding_index, current_edge[1]});
            p.elist[i][1] = colliding_index;
            // no increment so this path gets checked
        } else {
            plist.push_back(new_path);
            i++;
        }
    }
}

void path::generate_paths(triangulation &triang) {
    plist = triang.plist;
    auto &plist_func = plist;

    std::vector<edge> unique_edges;

    for (triangle curr_tri : triang.tlist) {
        for (unsigned int i = 0; i < 3; i++) {
            auto index_one = curr_tri[i];
            auto index_two = curr_tri[(i + 1) % 3];
            vec2 connection = (plist[index_one] - plist[index_two]);
            unique_edges.push_back({
                    index_one, index_two,
                    std::abs(connection.dot(connection))});
        }
    }

    // TODO check this assumption
    // sort backwards because then the smaller edges are at the end
    // where it is faster to erase elements because that is what we
    // are doing when we find a connecting edge with the smallest weight
    std::sort(unique_edges.rbegin(), unique_edges.rend());

    // remove all duplicate edges
    unique_edges.erase(std::unique(unique_edges.begin(), unique_edges.end()),
            unique_edges.end());

    // add first edge and remove it from the unique_edges vector
    // so it can't be added twice
    elist.push_back(unique_edges.back());
    unique_edges.pop_back();

    do {
        auto curr_edge_it = elist.begin();
        auto possible_connection = unique_edges.begin();
        edge curr_edge = *curr_edge_it;
        edge possible_edge = *possible_connection;

        auto minimal_connection = unique_edges.end();
        float min_weight = std::numeric_limits<float>::max();

        while (curr_edge_it != elist.end()) {
            possible_connection = unique_edges.begin();
            curr_edge = *curr_edge_it;

            while (possible_connection != unique_edges.end()) {

                possible_connection = std::find_if(possible_connection,
                        unique_edges.end(),
                        [&curr_edge](const edge &e) {
                            return curr_edge.common_points(e);
                        });

                if (possible_connection == unique_edges.end())
                    break;

                possible_edge = *possible_connection;

                // an edge is valid if just one point is contained in the msp
                bool indices[2] = {};
                bool valid_edge = std::none_of(elist.begin(),
                        elist.end(),
                        [&possible_edge, &indices](const edge &e) {
                            if (possible_edge.common_points(e)) {
                                indices[possible_edge.sharing_index(e)] = true;
                            }
                            return indices[0] & indices[1];
                        });

                // check if the edge is valid
                // that means it has only one edge with the msp in common otherwise
                // it will generate a cycle
                // check if weight of possible_edge is less than the weight of
                // the current "best" edge to add
                if (valid_edge && (possible_edge.weight < min_weight)) {
                    min_weight = possible_edge.weight;
                    minimal_connection = possible_connection;
                }

                ++possible_connection;
            }

            ++curr_edge_it;
        }

        // nothing found
        if (minimal_connection == unique_edges.end())
            break;

        elist.push_back(*minimal_connection);
        unique_edges.erase(minimal_connection);

    } while (true);
}
